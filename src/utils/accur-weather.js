const settings = require('../../configs/settings');

const axios = require('axios');

const enpoint = 'http://dataservice.accuweather.com/';

function getCurrentCoditionLink(locationKey) {
  const currentCondition = 'currentconditions/v1/';
  return currentCondition + locationKey
}

function getAccurWeatherKey (number) {
  let index = 0;
  if (!isNaN(parseInt(number))) {
    index = number;
  }

  let totalKey = settings.AWS_KEY.length;
  if (index > totalKey - 1) {
    return null;
  }
  else {
    return settings.AWS_KEY[index];
  }
}

async function getWeatherOfLocation(locationKey, pos) {
  let result = {
    text: '',
    icon: -1,
    temperature: '',
    unit: ''
  };

  try {
    const link = getCurrentCoditionLink(locationKey);
    let key = getAccurWeatherKey(pos);

    if (key !== null) {
      let params = {
        apikey: key
      };

      let res = await callAPI('get', link, {}, params);
      if (res !== null) {
        if (res.length === 1) {
          result.text = res[0]['WeatherText'];
          result.icon = res[0]['WeatherIcon'];
          result.temperature = res[0]['Temperature']['Metric']['Value'];
          result.unit = res[0]['Temperature']['Metric']['Unit'];
          return result;
        }
      }
    }

    pos += 1;
    return await getWeatherOfLocation(locationKey, pos);
  } catch (e) {
    console.log('Error when fetching data from response: ', e);
    return result;
  }
}

async function callAPI(method, url, data, params) {
  try {
    let options = {
      method: method,
      baseURL: enpoint,
      url: url,
      data: data,
      params: params
    };
    console.log('> call API options: ', options);
    let res = await axios(options);
    if (res.status === 200 && res.statusText === 'OK') {
      return res.data;
    }
  } catch (e) {
    console.log('Call API error: ', e.message);
    return null;
  }
}

function getDayAndNightWeather() {
  return [
    'cloudy', 'dreary (overcast)', 'fog', 'showers', 't-storms', 'rain', 'flurries', 'snow', 'ice', 'sleet',
    'freezing rain', 'rain and snow', 'hot', 'cold', 'windy',
  ]
}

function getDayWeather() {
  return [
    'clear',
    'mostly clear',
    'partly cloudy',
    'intermittent clouds',
    'hazy moonlight',
    'mostly cloudy',
    'partly cloudy w/ showers',
    'mostly cloudy w/ showers',
    'partly cloudy w/ t-storms',
    'mostly cloudy w/ t-storms',
    'mostly cloudy w/ flurries',
    'mostly cloudy w/ snow',
  ]
}

function getNightWeather() {
  return [
    'sunny',
    'mostly sunny',
    'partly sunny',
    'intermittent clouds',
    'hazy sunshine',
    'mostly cloudy',
    'mostly cloudy w/ showers',
    'partly sunny w/ showers',
    'mostly cloudy w/ t-storms',
    'partly sunny w/ t-storms',
    'mostly cloudy w/ flurries',
    'partly sunny w/ flurries',
    'mostly cloudy w/ snow',
  ]
}

function getWeatherCategory(iconWeather) {
  // https://developer.accuweather.com/weather-icons
  if (iconWeather > 0 && iconWeather < 5) {
    return 'Nắng';
  } else if (iconWeather > 4 && iconWeather < 12) {
    return 'Mây';
  } else if (iconWeather > 11 && iconWeather < 22 || iconWeather > 38 && iconWeather < 45) {
    return 'Mưa';
  } else if (iconWeather > 21 && iconWeather < 30) {
    return 'Tuyết';
  } else if (iconWeather > 31 && iconWeather < 39) {
    return 'Mây';
  } else if (iconWeather === 30) {
    return 'Nắng';
  } else if (iconWeather === 31) {
    return 'Mây';
  } else {
    let now = new Date();
    let hour = now.getHours();
    if (hour >= 6 && hour < 18) {
      return 'Nắng';
    } else {
      return 'Mây';
    }
  }
}

module.exports = {
  getWeatherOfLocation,
  getWeatherCategory
};

