var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/grab-scraper');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  var Schema = new mongoose.Schema({
    city: String,
    district: String,
    from: String,
    to: String,
    book_time: Date,
    book_price: String,
    book_price_currency: {type: String, default: 'VNĐ'},
    available_car: Number,
    promo_code: String,
    from_keyword: String,
    to_keyword: String
  }, {
    timestamps: true
  });

  var Record = mongoose.model('Record', Schema);

  module.exports = Record;
});







