const moment = require('moment');

const dbUtils = require('./utils/db');
const weatherUtils = require('./utils/accur-weather');
const grabCrawler = require('./grab-crawler');
const stringUtils = require('./utils/stringUtils');
const settings = require('../configs/settings');

function genRandomNumber(max) {
  return Math.floor((Math.random() + 1) * max);
}

async function execute() {
  let inputData = await dbUtils.readData(settings.DB_DISTANCE_COLLECTION, {}, {});
  console.log('> Input data: ', inputData);

  let weather = await weatherUtils.getWeatherOfLocation(settings.AW_LOCATION_KEY_HCM, 0);
  console.log('> Input weather: ', weather);

  const deviceId = settings.DEVICE_03;

  let outputData = [];
  let totalInput = inputData.length;
  if (totalInput > 0) {
    for (let i = 0; i < totalInput; i++) {
      let data = inputData[i];
      let result = Object.assign({}, data);
      console.log(`>>>>>>>>>>> Begin crawl data from=${data.from}, to=${data.to}:`);
      const category = settings.CATEGORIES.CAR;
      const vehicleType = settings.CAR_TYPE.CAR_4_SEATS.text;

      let newFrom = stringUtils.convertToNonUnicode(data.from);
      let newTo = stringUtils.convertToNonUnicode(data.to);
      let crawledResult = await grabCrawler(newFrom, newTo, data['distance_id'], deviceId, category, vehicleType);

      if (result.city === 'HCM') {
        result.city = 'Hồ Chí Minh';
      }
      if (result.city === 'HN') {
        result.city = 'Hà Nội';
      }
      result.district = result.district.toString();

      if (crawledResult != null) {
        result.date = moment(crawledResult.crawling_time).format('YYYY-MM-DD');
        result.time = moment(crawledResult.crawling_time).format('HH:mm:ss');
        result.book_time = new Date(crawledResult.crawling_time);
        result.book_price = parseInt(crawledResult.price.replace('K', '000'));
        result.book_price_currency = crawledResult.currency;
        result.promo_code = crawledResult.promote_code;
        result.promo_price = parseInt(crawledResult.booking_price.replace('K', '000'));
        result.price = parseFloat(result.book_price / result.distance_by_car).toFixed(2);
        result.raw_weather = weather.text;
        result.weather = weatherUtils.getWeatherCategory(weather.icon);
        result.available_car = crawledResult.density;
        result.payment = crawledResult.payment;
        result.membership = crawledResult.membership;
        result.fare_type = crawledResult.fare_type;
        result.vehicle_type = crawledResult.vehicle_type;
        result.distance_type = result.distance_by_car >= 3 ? 'Long' : 'Short';

        result.from_keyword = crawledResult.from;
        result.to_keyword = crawledResult.to;

        delete result._id;
        delete result.id;
        delete result.__v;

        // Su dung time cua turn crawl ke tiep de insert data;
        if (i < totalInput - 1) {
          dbUtils.writeData(settings.DB_RECORD_COLLECTION, result);
        } else {
          await dbUtils.writeData(settings.DB_RECORD_COLLECTION, result);
        }
        outputData.push(result);
      }

      let second = genRandomNumber(10);
      console.log(`>>>>>>>>>>> Wait for ${second} seconds....`);
      if (i < totalInput - 1) {
        await waitForMillisecond(second * 1000);
      }
    }
  }

  console.log('> Output data: ', outputData);
}

function waitForMillisecond(value) {
  return new Promise(resolve => {
    let seconds = value / 1000;
    setTimeout(() => {
      console.log(`Done for waiting ${seconds} seconds.`);
      resolve(true);
    }, value, `Done for waiting ${seconds} seconds.`);
  });
}

execute()
  .then(() => {
    process.exit();
  })
  .catch(error => {
    console.log('[execute] Error when executing crawl: ', error);
    return waitForMilisecond(1000 * 5)
      .then(() => {
        process.exit();
      });
  });
