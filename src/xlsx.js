const path = require('path');
const fs = require('fs');
const XLSX = require('xlsx');

const ROW_FORMAT_INPUT = [
  {column: 'A', code: 'city', name: 'Thành phố'},
  {column: 'B', code: 'district', name: 'Quận'},
  {column: 'C', code: 'from', name: 'Điểm đi'},
  {column: 'D', code: 'to', name: 'Điểm đến'}
];

const ROW_FORMAT_OUTPUT = [
  {column: 'A', code: 'city', name: 'Thành phố'},
  {column: 'B', code: 'district', name: 'Quận'},
  {column: 'C', code: 'from', name: 'Điểm đi'},
  {column: 'D', code: 'to', name: 'Điểm đến'},
  {column: 'E', code: 'book_time', name: 'Giờ đặt xe'},
  {column: 'F', code: 'book_price', name: 'Giá đặt'},
  {column: 'G', code: 'available_car', name: 'Lượng xe'},
  {column: 'H', code: 'promo_code', name: 'Mã khuyến mãi'},
  {column: 'I', code: 'from_keyword', name: 'Điểm đi - tìm kiếm'},
  {column: 'J', code: 'to_keyword', name: 'Điểm đến - tìm kiếm'},
];

async function readData(inputFile, inputSheetName) {
  // let inputFile = await getInputFile();
  let workbook = XLSX.readFile(inputFile);
  let inputSheet = workbook.Sheets[inputSheetName];

  let data = [];

  let maxEmptyRow = 10;
  let totalEmptyRows = 0;
  let totalProcessRows = 0;
  let next = true;
  let nextRow = 1;

  while (next) {
    let object = {
      city: '',
      district: '',
      from: '',
      to: ''
    };

    ROW_FORMAT_INPUT.map(row => {
      let cellAddress = generateAddress(row.column, nextRow);
      let cell = inputSheet[cellAddress];
      let cellValue = cell ? cell.v : undefined;
      if (cellValue) {
        if (cellValue.toString().toLowerCase().indexOf(row.name.toLowerCase()) === -1) {
          switch (row.code) {
            case 'city' :
              object.city = cellValue;
              break;
            case 'district':
              object.district = cellValue;
              break;
            case 'from':
              object.from = cellValue;
              break;
            case 'to':
              object.to = cellValue;
              break;
          }
        }
      }
    });

    if (object.city === '' && object.district === '' && object.from === '' && object.to === '') {
      totalEmptyRows += 1;
    }
    else {
      data.push(object);
      totalProcessRows += 1;
    }
    if (totalEmptyRows === maxEmptyRow) {
      next = false;
    }
    if (next) {
      nextRow += 1;
    }
  }

  return data;
}

async function writeData(data, outputFile, sheetName) {
  console.log('> data: ', data);
  console.log('> outputFile: ', outputFile);
  console.log('> sheetName: ', sheetName);

  let dataToSaved = [];
  if (data && !Array.isArray(data)) {
    dataToSaved.push(data);
  }
  else {
    dataToSaved = dataToSaved.concat(data);
  }
  // TODO check if output file is exists, if not, must create new file.
  let workbook = XLSX.readFile(outputFile);

  let noHeaders = ROW_FORMAT_OUTPUT.map(row => {
    return row.column;
  });
  let firstHeaderRow = {};
  console.log(">>> noHeaders: ", noHeaders);

  if (workbook.Sheets[sheetName] === undefined) {
    ROW_FORMAT_OUTPUT.map(row => {
      firstHeaderRow[`${row.column}`] = row.name;
    });
    console.log(">>> firstHeaderRow: ", firstHeaderRow);
    let newWorkSheet = XLSX.utils.json_to_sheet([firstHeaderRow], {header: noHeaders, skipHeader: true});

    XLSX.utils.book_append_sheet(workbook, newWorkSheet, sheetName);
    console.log('====== workbook: ', workbook.SheetNames);
    console.log(`Added new worksheet name ${sheetName} successfully!`);
  }

  let workSheet = workbook.Sheets[sheetName];
  // add data to sheet
  let jsonObjects = dataToSaved.map(data => {
    let obj = {};
    ROW_FORMAT_OUTPUT.map(row => {
      obj[`${row.column}`] = data[row.code];
    });
    return obj;
  });

  console.log('jsonObjects: ', jsonObjects);
  XLSX.utils.sheet_add_json(workSheet, jsonObjects, {header: noHeaders, skipHeader: true, origin: -1});
  XLSX.writeFile(workbook, outputFile);
  console.log(`Write data to workbook successfully!`);
  return true;
}

async function readCrawledData(inputFile, inputSheetName) {
  let workbook = XLSX.readFile(inputFile);
  let inputSheet = workbook.Sheets[inputSheetName];

  let data = [];

  let maxEmptyRow = 10;
  let totalEmptyRows = 0;
  let totalProcessRows = 0;
  let next = true;
  let nextRow = 1;

  while (next) {
    let object = {
      city: '',
      district: '',
      from: '',
      to: '',
      book_time: '',
      book_price: '',
      book_price_currency: 'VNĐ',
      available_car: 0,
      promo_code: '',
      from_keyword: '',
      to_keyword: ''
    };

    ROW_FORMAT_OUTPUT.map(row => {
      let cellAddress = generateAddress(row.column, nextRow);
      let cell = inputSheet[cellAddress];
      let cellValue = cell ? cell.v : undefined;
      if (cellValue) {
        if (cellValue.toString().toLowerCase().indexOf(row.name.toLowerCase()) === -1) {
          switch (row.code) {
            case 'city' :
              object.city = cellValue;
              break;
            case 'district':
              object.district = cellValue;
              break;
            case 'from':
              object.from = cellValue;
              break;
            case 'to':
              object.to = cellValue;
              break;
            case 'book_time':
              object.book_time = cellValue;
              break;
            case 'book_price':
              object.book_price = cellValue;
              break;
            case 'book_price_currency':
              object.book_price_currency = cellValue;
              break;
            case 'available_car':
              object.available_car = cellValue;
              break;
            case 'promo_code':
              object.promo_code = cellValue;
              break;
            case 'from_keyword':
              object.from_keyword = cellValue;
              break;
            case 'to_keyword':
              object.to_keyword = cellValue;
              break;
          }
        }
      }
    });

    if (object.city === '' && object.district === '' && object.from === '' && object.to === '') {
      totalEmptyRows += 1;
    }
    else {
      data.push(object);
      totalProcessRows += 1;
    }
    if (totalEmptyRows === maxEmptyRow) {
      next = false;
    }
    if (next) {
      nextRow += 1;
    }
  }

  return data;
}

function generateAddress(columnIndex, rowIndex) {
  return columnIndex + rowIndex
}

module.exports.readData = readData;
module.exports.writeData = writeData;
module.exports.readCrawledData = readCrawledData;
