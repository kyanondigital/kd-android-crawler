const MongoClient = require('mongodb').MongoClient;
const settings = require('../configs/settings');

async function connectDB() {
    const client = new MongoClient(settings.DB_CONNECTION_STRING_PROD);
    await client.connect();
    return client.db(settings.DB_NAME);
}

async function execute() {
    try {
        let db = await connectDB();
        let recordCollection = db.collection(settings.DB_RECORD_COLLECTION);
        let distances = await db.collection(settings.DB_DISTANCE_COLLECTION).find().toArray();

        let records = await recordCollection.find({distance_id: {$exists: false}}).toArray();

        let jobs = records.map(async record => {
            let distance = distances.find(e => {
                return e.from === record.from && e.to === record.to
            });
            if (distance !== undefined) {

                let price = parseFloat(record.book_price / distance['distance_by_car']).toFixed(2);

                await recordCollection.update({_id: record._id}, {
                    $set: {
                        price: price,
                        distance_by_car: distance['distance_by_car'],
                        distance_unit: distance['distance_unit'],
                        distance_id: distance['distance_id'],
                    }
                });
            }
        });

        await Promise.all(jobs);
        console.log('Import data successfully!');
    }
    catch (err) {
        console.log('Error: ', err);
    }
}


execute();